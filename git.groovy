import groovy.transform.Field

    @Field WorkflowScript context

    WorkflowScript init(WorkflowScript context) {
        this.context = context
        return this
    }

@Field String __buildHash
     String getbuildHash() {    
            __buildHash = sh returnStdout: true, script: "pwd"
            return __buildHash
            
        }
return this